import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {ConfiguracionService} from '../_services/configuration.service'
import { Employee } from '../_models/employee';
@Injectable({
  providedIn: 'root'
})

export class ApicallService {

  constructor(private httpClient: HttpClient, private urlRoot : ConfiguracionService) { }

  findAll(): Observable<any>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    return this.httpClient.get(
      this.urlRoot.rootURL,
       {headers: headers}
      ).pipe(
           map((data) => {
             return data;
           }), catchError( error => {
             return throwError(error);
           })
        )
    }

    findById(id:number): Observable<any>{
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Accept', 'application/json');
      return this.httpClient.get(
        this.urlRoot.rootURL+id,
         {headers: headers}
        ).pipe(
             map((data) => {
               return data;
             }), catchError( error => {
               return throwError( error );
             })
          )
      }

    CreateEmployee(employee): Observable<any>{
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Accept', 'application/json');
      return this.httpClient.post(
        this.urlRoot.rootURL, employee ,
         {headers: headers}
        ).pipe(
             map((data) => {
               return data;
             }), catchError( error => {
               return throwError(error );
             })
          )
      }

      UdateEmployee(employee,id): Observable<any>{
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Accept', 'application/json');
        return this.httpClient.put(
          this.urlRoot.rootURL + id, employee ,
           {headers: headers}
          ).pipe(
               map((data) => {
                 return data;
               }), catchError( error => {
                 return throwError(error );
               })
            )
        }

    DeleteEmployee(id:number): Observable<any>{
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Accept', 'application/json');
      return this.httpClient.delete(
        this.urlRoot.rootURL + id ,
         {headers: headers}
        ).pipe(
             map((data) => {
               return data;
             }), catchError( error => {
               return throwError(error);
             })
          )
      }

}
