import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from './components/list/list.component'
import {CreateUpdateComponent} from './components/create-update/create-update.component'

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'create', component: CreateUpdateComponent},
  { path: 'update', component: CreateUpdateComponent},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const AppRoutingModule = RouterModule.forRoot(routes);
