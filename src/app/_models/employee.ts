export interface Employee {
    id:number;
    name: String ;
    second_name:String;
    surmane:String;
    second_surname:String;
    email:String;
    country_work:String;
    area:String;
    document:String;
    document_type:String;
    record_add:String;
    record_update:String;
    hire_date:String;
    state:Boolean;
  }
