import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ApicallService } from 'src/app/_services/apicall.service';
import { Employee } from 'src/app/_models/employee';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.css']
})
export class CreateUpdateComponent implements OnInit {
  constructor(private route: ActivatedRoute,
    private service: ApicallService,
    private toastr: ToastrService,
    private fb: FormBuilder,) {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.maxDate = this.getDate(this.maxDate);
    this.minDate = this.getminDate(this.minDate);
    console.log(this.minDate)
  }
  minDate: any;
  maxDate: any;
  result: any;
  create = true;
  submitting = true;
  employee: Employee;
  EmployeeForm: FormGroup;
  tituloEntrada = "Agregar empleado"
  queryId = this.route.queryParamMap
    .subscribe((params) => {
      this.result = { ...params.keys, ...params };
    }
    );
  ngOnInit(): void {
    if (this.result.params.id) {
      this.tituloEntrada = "Modificar empleado"
      this.create = false;

      this.service.findById(this.result.params.id).subscribe((res: any) => {
        console.log(res)
        this.EmployeeForm = this.fb.group({
          id: [res.id],
          name: [res.name, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
          second_name: [res.second_name, [Validators.minLength(5), Validators.maxLength(50)]],
          surmane: [res.surmane, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
          second_surname: [res.second_surname, [Validators.minLength(5), Validators.maxLength(20)]],
          email: [res.email, [Validators.max(300)]],
          country_work: [res.country_work, [Validators.required]],
          area: [res.area, [Validators.required]],
          document: [res.document, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
          document_type: [res.document_type, [Validators.required]],
          record_add: [res.record_add],
          record_update: [res.record_update],
          hire_date: [res.hire_date, [Validators.required]],
          state: [res.state],
        });
      })



    } else {
      this.EmployeeForm = this.fb.group({
        id: [''],
        name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]],
        second_name: ['', [Validators.minLength(5), Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]],
        surmane: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]],
        second_surname: ['', [Validators.minLength(5), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]],
        email: ['', [Validators.max(300)]],
        country_work: ['', [Validators.required]],
        area: ['', [Validators.required]],
        document: [, [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9_]+( [a-zA-Z0-9_]+)*$')]],
        document_type: ['', [Validators.required]],
        record_add: [''],
        record_update: [''],
        hire_date: ['', [Validators.required]],
        state: new FormControl({ value: true, disabled: true }),
      });
    }

  }

  get f() {
    return this.EmployeeForm.controls;
  }


  getminDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth()),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');

  }

  getDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');

  }

  SeeData() {
    console.log(this.EmployeeForm.value);
    this.getDate(this.EmployeeForm.value['hire_date'])
  }

  CreateEmployee() {
    if (this.EmployeeForm.valid) {
      if (this.create) {
        this.service.CreateEmployee(this.EmployeeForm.value)
          .subscribe((res:any) => {
            console.log(res)
              if(res){
                this.toastr.success("El usuarios fue registrado con éxito", "")
                this.EmployeeForm.reset()
              }
          });
      }
      else {
        this.service.UdateEmployee(this.EmployeeForm.value, this.EmployeeForm.value['id'])
          .subscribe((res: any) => {
            console.log('res :>> ', res['status']);
            if (res) {
              console.log(res)
              this.toastr.success("El usuarios fue modificado con éxito", "")
              location.reload();
            }
          });
      }
    }
  }
}
