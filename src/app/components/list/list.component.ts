import { Component,OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {Employee } from '../../_models/employee';
import {ApicallService} from '../../_services/apicall.service'
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit,OnDestroy {
  dtOptions: DataTables.Settings = {};
  data:Employee[] = [];
  table:any;
  dtTrigger = new Subject<any>();
  constructor(private service:ApicallService,private toastr:ToastrService) { }

  ngOnInit(): void {
    console.log("entras")
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    scrollX:true,
    };
    this.GetAll();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  GetAll(){
    this.service.findAll().subscribe((res:any)=>{
      console.log(res)
      if(res){
        this.data = res
        this.dtTrigger.next()
      }
      else{
        this.toastr.error('Recarga la pagina por favor', 'Algo salió mal!');
      }

    })
  }

  Delete(id){
    this.toastr.warning("Click para continuar",'Eliminar usuario?',)
    .onTap
    .pipe(take(1))
    .subscribe(() => {
      this.eliminarUsuario(id)
    })
  }

  eliminarUsuario(id) {
  this.service.DeleteEmployee(id).subscribe((res:any)=>{
      location.reload()
    });
  }

}
